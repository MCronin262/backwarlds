﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {

    public static GameManager instance;

    public int score = 0;
    public float timer=60;
    public bool gameOver;
    public Text timerText;
    public Text menuText;
    public Text scoreText;
    public Text hiScoreText;
    public GameObject menu;
    public GameObject menuImage;
    public Text smallMenuText;
    public string trigger = "swish";
    public string trogger = "swoosh";
    public Animator anim;
    public ParticleSystem blood;
    public bool hasSpunBack=true;


    private void Awake()
    {
        if (instance == null) { instance = this; }
    }


    // Use this for initialization
    void Start () {
        gameOver = true;
	}
	
	// Update is called once per frame
	void Update () {

        timerText.text = timer.ToString("0.0");
        scoreText.text = "Score: "+score ;



        timer -= Time.deltaTime;

        if (timer < 0) { gameOver = true; }

        if (gameOver)
        {

            if (!hasSpunBack)
            {
                hasSpunBack = true;
                anim.SetTrigger(trogger);
            }


            //  menuImage.SetActive(true);
            menuText.text = "HUNTING SEASON";
            smallMenuText.text = "Hit Space to Start";
            timerText.text = "";
            timer = 0;
            menu.SetActive(true);

            if (Input.GetButtonDown("Jump"))
            {
               // menu.SetActive(false);
                timer = 30;
                score = 0;
                gameOver = false;
                menuText.text = "";
                smallMenuText.text = "";
                menuImage.SetActive(false);
                anim.SetTrigger(trigger);
                hasSpunBack = false;
            }
    }

    int highScore = PlayerPrefs.GetInt("HighScore", 0);
        if (score > highScore)
        {
            PlayerPrefs.SetInt("HighScore", score);
            PlayerPrefs.Save();
            hiScoreText.text = "High Score: " + highScore;
        }
        hiScoreText.text = "High Score: " + highScore;
    }
}
