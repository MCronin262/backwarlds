﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

    public static PlayerController instance;

    public Rigidbody body;
    public float speed;
    public GameObject rifle;
    public Transform riffle;


    private void Awake()
    {
        if (instance == null) { instance = this; }
    }

    private void Start()
    {
        body = GetComponent<Rigidbody>();
    }
    private void Update()
    {
        float x = Input.GetAxis("Horizontal");
       // float y = Input.GetAxis("Vertical");

        Vector3 heading = new Vector3(x, 0, 0);
        Vector3 velocity = heading * speed;
        transform.position = transform.position + velocity * Time.deltaTime;

        if (Input.GetButtonDown("Jump")) {
            GameObject bullet = Spawner.instance.Spawn("Bullet");
            bullet.transform.position = riffle.position;
            AudioManager.instance.PlaySFX("gunShot");
        }


    }
    
}
