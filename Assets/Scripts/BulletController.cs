﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletController : MonoBehaviour {

    public static BulletController instance;

    public Rigidbody body;
    public float speed=20;


    private void Awake()
    {
        if (instance == null) { instance = this; }
    }


    // Use this for initialization
    void Start () {
        body = GetComponent<Rigidbody>();
        gameObject.transform.position = PlayerController.instance.rifle.transform.position;
    }
	
	// Update is called once per frame
	void Update () {

        
        StartCoroutine(Pew());
       

    }

    private void OnCollisionEnter(Collision c)
    {
        if (c.gameObject.tag == "Human") {
            GameManager.instance.score += 100;
            gameObject.SetActive(false);
            GameManager.instance.blood.transform.position = c.gameObject.transform.position;
            GameManager.instance.blood.Play();
            AudioManager.instance.PlaySFX("die1");
        }
    }



    IEnumerator Pew() {

        while (enabled)
        {
            
            Vector3 heading = new Vector3(0, 0, 5);
            body.velocity = heading * speed;
            yield return new WaitForSeconds(0.2f);
            StartCoroutine(Vanish());
        }
    }

    IEnumerator Vanish()
    {
        yield return new WaitForSeconds(0.5f);
        gameObject.SetActive(false);
    }




    void OnTriggerEnter(Collider other)
    {
        //gameObject.SetActive(false);

    }
}
